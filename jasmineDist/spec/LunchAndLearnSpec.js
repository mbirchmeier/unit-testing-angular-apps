it("should succeed", function() {
    expect(true).toBe(true);
  });
  
describe("feed service", function() {
	var feedservice;
	var http;
	
	beforeEach(function(){
		module('RSSFeedApp'); // activate the module
		
		inject(function (_FeedService_, $http) {
			http = $http;
			feedservice = _FeedService_;
		});
	});

	
	it("should load", function() {
		expect(feedservice).not.toBe(undefined);
	});
	
	it("should call jsonp", function() {
		spyOn(http, 'jsonp');  //note we need to add this to the injector
		feedservice.parseFeed('someurl');
		expect(http.jsonp).toHaveBeenCalled();
		expect(http.jsonp.calls.mostRecent().args[0]).toEqual('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=someurl');
	});
});

describe('feed controller', function() {
	var $scope;
	var $controller;
	
	var mockReturned = {
			data: {
				responseData: {
					feed: {
						entries: []
					}
				}
			}
		};
	var mockFeedService = {
		parseFeed: mockThen(mockReturned)
	}
	
	beforeEach(function() {
		module('RSSFeedApp');
		
		module(function ($provide) {
            $provide.value('FeedService', mockFeedService);
        });
		
		inject(function (_$rootScope_, _$controller_) {
			// The injector unwraps the underscores (_) from around the parameter names when matching
			$controller = _$controller_;
			$scope = _$rootScope_.$new();
			$controller('FeedCtrl', { $scope: $scope });
		});
	

		

	});
	it('loads the scope', function() {
		expect($scope.loadButonText).toBe("Load");	
	});
	
	it('our mock gets called', function() {
		spyOn(mockFeedService, 'parseFeed').and.callThrough();
		$scope.loadFeed();
		expect(mockFeedService.parseFeed).toHaveBeenCalled();
		expect(mockReturned.data.responseData.feed.entries).toBe($scope.feeds);
	})
});