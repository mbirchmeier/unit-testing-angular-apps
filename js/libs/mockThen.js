var mockThen = function (params) {
    return function (functionParam) {
        return {
            then: function (successFunction, failureFunction) {
                //if we have a success value we have an advanced mock object
                if (params.successValue || params.successFunction) {
                    if (params.returnFailure) {
                        var failureValue = {};
                        if (params.failureValue) {
                            failureValue = params.failureValue
                        }
                        if (params.failureFunction) {
                            failureValue = params.failureFunction(functionParam);
                        }

                        return failureFunction(successValue);
                    } else {
                        var successValue = {};
                        if (params.successValue){
                            successValue = params.successValue
                        }
                        if (params.successFunction) {
                            successValue = params.successFunction(functionParam);
                        }

                        return successFunction(successValue);
                    }
                } else {
                    //if we don't just return what we got
                    successFunction(params);
                }
            }
        };
    };
};