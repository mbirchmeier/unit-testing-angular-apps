'use strict';
(function() {
	var App = angular.module('RSSFeedApp');


	App.controller("FeedCtrl", ['$scope','FeedService', function ($scope,Feed) {    
		$scope.loadButonText="Load";
		$scope.loadFeed=function(e){        
			Feed.parseFeed($scope.feedSrc).then(function(res){
				$scope.feeds=res.data.responseData.feed.entries;
			});
		}
	}]);
})();

