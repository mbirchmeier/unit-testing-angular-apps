::createFirstTest::
	send,it('should fail', function() {{} {Enter}
	send,{Tab}expect(true).toBe(false); {Enter}
	send,{BS}{}});{Enter}
Return

::createMultiply::
	send,var Multiply = function(a,b) {{} {Enter}
	send,{Tab}return a * b;{Enter}
	send,{BS}{}};{Enter}
	send,{Enter}
	send,var PromptAndMultiply = function(){{} {Enter}
	send,{Tab}var a = parseInt(prompt('number 1'));{Enter}
	send,var b = parseInt(prompt('number 2'));{Enter}
	send,return Multiply(a,b);{Enter}
	send,{BS}{}};{Enter}
Return

::describeMultiply::
	send,describe('Multiplication', function() {{}{Enter}
	send,{}});{Enter}
Return

::describeFeedService::
	send,describe('feed service', function() {{}{Enter}
	send,{}});{Enter}
Return

::describeFeedController::
	send,describe('feed controller', function() {{}{Enter}
	send,{}});{Enter}
Return

::positiveMultiply::
	send,it('of two positive numbers', function() {{}{Enter}
	send,expect(Multiply(2,2)).toBe(4);{Enter}
	send,{BS}{}});{Enter}
Return

::negativeMultiply::
	send,it('of two negative numbers', function() {{}{Enter}
	send,expect(Multiply(-2,-2)).toBe(4);{Enter}
	send,{BS}{}});{Enter}
Return

::promptMultiply::
	send,it('prompted numbers', function() {{}{Enter}
	send,expect(PromptAndMultiply()).toBe(4);{Enter}
	send,{BS}{}});{Enter}
Return

::multiplyMock::
	send,beforeEach(function() {{}{Enter}
	send,window.prompt = function() {{}{Enter}
	send,return 2;{Enter}
	send,{BS}{}}{Enter}
	send,{BS}{}});
Return

::multiplySpy::
	send,beforeEach(function() {{}{Enter}
	send,spyOn(window, 'prompt');	 {Enter}
	send,{BS}{}});
Return

::angularSpecs::
  send,<script src="http://localhost:8080/js/libs/angular.min.js"></script>{Enter}
  send,<script src="http://localhost:8080/js/libs/angular-mocks.js"></script>{Enter}
  send,<script src="http://localhost:8080/js/libs/bootstrap.min.js"></script>{Enter}
  send,<script src="http://localhost:8080/js/libs/mockThen.js"></script>{Enter}
  send,{Enter}
  send,<{!}-- include source files here... -->{Enter}
  send,<script src="http://localhost:8080/js/modules/RSSFeedApp.js"></script>{Enter}
  send,<script src="http://localhost:8080/js/Services/FeedService.js"></script>{Enter}
  send,<script src="http://localhost:8080/js/Controllers/FeedCtrl.js"></script>{Enter}
Return

::activateModule::
  send,beforeEach(function(){{}{Enter}
  send,module('RSSFeedApp'); // activate the module {Enter}
  send,{BS}{}});{Enter}
Return

::injectFeedService::
  send,beforeEach(inject(function(_FeedService_){{}{Enter}
  send,feedservice = _FeedService_;{Enter}
  send,{BS}{}}));{Enter}
Return

::feedServiceNotUndefined::
	send,it('feed service should be loaded', function() {{}{Enter}
	send,expect(feedservice).not.toBe(undefined);{Enter}
	send,{BS}{}});{Enter}
Return

::spyOnJsonp::
	send,it('Should call jsonp', function() {{}{Enter}
	send,spyOn(http, 'jsonp');{Enter}
	send,feedservice.parseFeed('someurl');{Enter}
	send,expect(http.jsonp).toHaveBeenCalled();{Enter}
	send,expect(http.jsonp.calls.mostRecent().args[0]).toEqual('//ajax.googleapis.com/ajax/services/feed/load?v=1.0&num=50&callback=JSON_CALLBACK&q=someurl');{Enter}
	send,{BS}{}});{Enter}
Return

::mockReturnedObject::
	send,var mockReturned = {{}{Enter}
	send,data: {{}{Enter}
	send,responseData: {{}{Enter}
	send,feed: {{}{Enter}
	send,entries:[]{Enter}
	send,{BS}{}}{Enter}
	send,{BS}{}}{Enter}
	send,{BS}{}}{Enter}
	send,{BS}{}};{Enter}
Return

::mockFeedService::
	send,var mockFeedService = {{}{Enter}
	send,parseFeed: mockThen(mockReturned){Enter}
	send,{BS}{}}
Return

::provideServices::
	send,module(function($provide) {{}{Enter}
	send,$provide.value('FeedService', mockFeedService);{Enter}
	send,{BS}{}});{Enter}
Return

::injectController::
	send,inject(function (_$rootScope_, _$controller_) {{}{Enter}
	send,// The injector unwraps the underscores (_) from around the parameter names when matching{Enter}
	send,$controller = _$controller_;{Enter}
	send,$scope = _$rootScope_.$new();{Enter}
	send,$controller('FeedCtrl', {{} $scope: $scope {}});{Enter}
	send,{BS}{}});{Enter}
Return

::testController::
	send,it('verify entries get added to the scope', function() {{}{Enter}
	send,spyOn(mockFeedService, 'parseFeed').and.callThrough();{Enter}
	send,$scope.loadFeed();{Enter}
	send,expect(mockFeedService.parseFeed).toHaveBeenCalled();{Enter}
	send,expect(mockReturned.data.responseData.feed.entries).toBe($scope.feeds);{Enter}
	send,{BS}{}});{Enter}
Return